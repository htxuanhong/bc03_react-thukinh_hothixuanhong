import React, { Component } from "react";
import "./BaiTapThuKinh.css";
import dataGlasses from "./dataGlasses.json";

export default class BaiTapThuKinh extends Component {
  state = {
    urlImg: "./glassesImage/model.jpg",
    selectedItem: null,
    glassesArr: dataGlasses,
  };

  _handleChangeGlasses = (item) => {
    this.setState({ selectedItem: item });
  };

  render() {
    const appStyle = {
      backgroundImage: "url(./glassesImage/background.jpg)",
      "background-size": "cover",
    };
    console.log(this.name);
    return (
      <div className="thu-kinh-bg" style={appStyle}>
        <div
          style={{
            position: "absolute",
            inset: 0,
            zIndex: 9,
          }}
        >
          <nav className="navbar navbar-light bg-nav justify-content-center text-white py-3">
            <h4>TRY GLASSES APP ONLINE</h4>
          </nav>
          <div className="container py-4">
            <div
              style={{
                position: "relative",
              }}
            >
              <img className="w-25 h-25  " src={this.state.urlImg} alt="" />
              {this.state.selectedItem && (
                <div>
                  <img
                    className=" vglasses__model  "
                    src={this.state.selectedItem?.url}
                    alt=""
                  />

                  <div className="vglasses__info">
                    <div className="text-primary">
                      {this.state.selectedItem?.name}
                    </div>
                    <div>{this.state.selectedItem?.desc}</div>
                  </div>
                </div>
              )}
            </div>

            <div className="border bg-light my-4">
              {this.state.glassesArr.map((item, index) => {
                return (
                  <img
                    key={index}
                    onClick={() => this._handleChangeGlasses(item)}
                    style={{ width: "150px", margin: "30px" }}
                    src={item.url}
                    alt=""
                  />
                );
              })}
            </div>
          </div>
        </div>
        <div
          className="back-drop"
          style={{
            "background-color": "black",
            position: "absolute",
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            opacity: 0.5,
          }}
        />
      </div>
    );
  }
}
